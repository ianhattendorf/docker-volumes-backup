FROM rclone/rclone

LABEL maintainer="Ian Hattendorf <ian@ianhattendorf.com>"

RUN set -eux; \
  apk --no-cache add gnupg xz sqlite \
  && gpg --batch --keyserver keyserver.ubuntu.com --recv-keys 97923DF85C294A7AE4058B253A7EA46F568616DC

ENTRYPOINT tar \
  -cvO \
  --exclude "backup/volumes/jellyfin_jellyfin_config/metadata" \
  --exclude "backup/volumes/portainer_data/backups" \
  --exclude "backup/volumes/scrypted_scrypted_data/plugins" \
  /backup/volumes \
  /backup/native/homeassistant_homeassistant_config/backups \
  /backup/native/arr_radarr_config/Backups/scheduled \
  /backup/native/arr_sonarr_config/Backups/scheduled \
  /backup/native/arr_prowlarr_config/Backups/scheduled \
  | nice -n 10 xz --threads=0 \
  | gpg --batch \
  --trust-model always \
  --encrypt \
  --compress-algo none \
  --recipient 97923DF85C294A7AE4058B253A7EA46F568616DC \
  --output /backup/volumes.tar.xz.gpg \
  && rclone --config /config/rclone.conf -v --bwlimit 300 \
  move /backup/volumes.tar.xz.gpg \
  "b2-docker-backup:docker-backup/volumes.$(date --utc "+%Y%m%d-%H%M%S").tar.xz.gpg" \
  && touch /backup/native/homeassistant_homeassistant_config/backups/sentinel \
  && touch /backup/native/arr_radarr_config/Backups/scheduled/sentinel \
  && touch /backup/native/arr_sonarr_config/Backups/scheduled/sentinel \
  && touch /backup/native/arr_prowlarr_config/Backups/scheduled/sentinel \
  && rm -f /backup/native/homeassistant_homeassistant_config/backups/* \
  && rm -f /backup/native/arr_radarr_config/Backups/scheduled/* \
  && rm -f /backup/native/arr_sonarr_config/Backups/scheduled/* \
  && rm -f /backup/native/arr_prowlarr_config/Backups/scheduled/*
