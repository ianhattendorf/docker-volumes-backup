# docker-volumes-backup

## Example systemd service + timer:

### docker-volumes-backup.service

```
[Unit]
Description=Docker volumes backup
Wants=docker-volumes-backup.timer

  # TODO games
  # TODO librenms
[Service]
Type=oneshot
ExecStart=/usr/bin/docker run --pull=always --rm \
  --volume arr_prowlarr_config:/backup/native/arr_prowlarr_config:rw \
  --volume arr_qbittorrent_config:/backup/volumes/arr_qbittorrent_config:ro \
  --volume arr_radarr_config:/backup/native/arr_radarr_config:rw \
  --volume arr_sonarr_config:/backup/native/arr_sonarr_config:rw \
  --volume grafana_grafana_data:/backup/volumes/grafana_grafana_data:ro \
  --volume homeassistant_homeassistant_config:/backup/native/homeassistant_homeassistant_config:rw \
  --volume homeassistant_zwave_js_config:/backup/volumes/homeassistant_zwave_js_config:ro \
  --volume jellyfin_jellyfin_config:/backup/volumes/jellyfin_jellyfin_config:ro \
  --volume mosquitto_mosquitto_data:/backup/volumes/mosquitto_mosquitto_data:ro \
  --volume portainer_data:/backup/volumes/portainer_data:ro \
  --volume scrypted_scrypted_data:/backup/volumes/scrypted_scrypted_data:ro \
  --volume snmpd_snmpd_data:/backup/volumes/snmpd_snmpd_data:ro \
  --volume traefik_traefik_acme:/backup/volumes/traefik_traefik_acme:ro \
  --volume traefik_traefik_conf:/backup/volumes/traefik_traefik_conf:ro \
  --volume weather-station-server_weather_station_server:/backup/volumes/weather-station-server_weather_station_server:ro \
  --volume rclone_config:/config:ro \
  registry.gitlab.com/ianhattendorf/docker-volumes-backup/docker-volumes-backup:main

[Install]
WantedBy=multi-user.target
```


### docker-volumes-backup.timer

```
[Unit]
Description=Docker volumes backup
Requires=docker-volumes-backup.service

[Timer]
Unit=docker-volumes-backup.service
OnCalendar=*-*-* 03:00:00

[Install]
WantedBy=timers.target
```